﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationBehaviour : MonoBehaviour
{

    GameObject AssetFound;
    GameObject TweenPathObj;
    GameObject StarParticaleObj;
    Animator CurrentAnimator;

    string PathName = "Path";
    string StarName = "StarParticle";

    //string PathName = "PathClone";
    //string StarName = "sign_1_Star";

    IEnumerator LoopPending;
    float PendingTime = 1.0f;

    // Use this for initialization
    void Start()
    {

        AssetFound = this.gameObject;
        CurrentAnimator = AssetFound.GetComponent<Animator>();
    }

    public void WaitingForTapping()
    {
        Debug.Log("StartLooping");


        CurrentAnimator.Play("Jump", 0, 0.48f);

        LoopPending = ReadyToFly();
        StartCoroutine(LoopPending);



    }
    IEnumerator ReadyToFly()
    {
        yield return new WaitForSeconds(PendingTime);
        StartFly();
    }

    void StartFly()
    {



        StarParticaleObj = GameObject.Find(StarName);
        ParticleSystem StarParticaleSys = StarParticaleObj.GetComponent<ParticleSystem>();
        StarParticaleSys.Play();



        Vector3[] paths = iTweenPath.GetPath(PathName);
       
        Hashtable moveHt = new Hashtable();
        moveHt.Add("path", paths);
        moveHt.Add("time", 1);
        moveHt.Add("speed", 25);
        moveHt.Add("onstart", "onTweenStart");
        //moveHt.Add("oncomplete", "onTweenComplete");
        //moveHt.Add("oncompletetarget", AssetFound);
        iTween.MoveTo(AssetFound, moveHt);

    }

    void onTweenStart()
    {
        Debug.Log("onTweenStart");

        CurrentAnimator.speed = 20.0f;


    }
}
