﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Vuforia;
using System.Linq;
using UnityEngine.Networking;
using GameAnalyticsSDK;


[System.Serializable]
public class Target
{

    public string tg_name;
    public string file_name;
    public string file_url;
    public string update_date;
    public string app_version;
}
public class Version
{
    public string app_version;
}
public class ImageTargetData
{

    public Target[] targets;
}
[System.Serializable]
public class DetectTable
{
    public string tg_name;
    public string detected;
}
[System.Serializable]
public class Setting
{
    public string tg_name;
    public string scale;
    public string shape_x;
    public string shape_y;
    public string shape_z;
}


public class CloudEyeManager : MonoBehaviour
{
    //---------GUI------------
    public bool GUIActive = false;
    Vector2 scrollPosition;
    GUIStyle style = new GUIStyle();
    public string LogMessage;

    // Use this for initialization
    public int version = 10;
    int serverVersion;
    public string domain = "http://cms.cloudeyelive.com:8080/";
    public string AppName = "KemofreZoo";
    string ImageTargetJsonFile = "ImageTarget.json";
    string DetectTableFile = "DetectTable.json";
    string VersionFile = "Version.txt";
    string SettingFile = "Setting.json";
    public Target[] imageTargets;
    public DetectTable[] detectTables;
    Setting[] settingList;
    Setting settingNode;

    bool ReseatDetectTable = false;

    string content_url;
    string bundle_content;
    bool vuforia_ready = false;
    AnalyticsManager analyticsManager;
    GameObject Loadingbar;

    public delegate void CloudEyeService();
    public static event CloudEyeService OnCloudEyeDataReady;




    void Start()
    {


        analyticsManager = this.gameObject.GetComponent<AnalyticsManager>();

    }



    void OnApplicationPause(bool pauseStatus)
    {
        bool isPaused = pauseStatus;

        if (vuforia_ready)
        {
            ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();

            if (isPaused)
            {

                objectTracker.Stop();
            }
            else
            {
                objectTracker.Start();
            }
        }


    }

    // Update All Setting
    public void DoReseat()
    {
        ReadAndSaveCurrentVersion();
        UpdateFileHandler();
    }

    public void CheckVersion()
    {

        LoadSettingFile();

        StartCoroutine(InitVersion());
       

    }
    IEnumerator InitVersion()
    {
        ReadAndSaveCurrentVersion();
        yield return null;
        StartCoroutine(GetVerionFromServer());

    }

    void ReadAndSaveCurrentVersion()
    {
        string filePath = Application.persistentDataPath + "/" + VersionFile;

        if (!File.Exists(filePath))
        {
            // No  Version File exists then creating
            StreamWriter newFile = new StreamWriter(filePath);

            newFile.WriteLine(version);
            newFile.Close();
        }
        else
        {

            // Version File exists
            string versionStr = File.ReadAllText(filePath);
            version = System.Convert.ToInt32(versionStr);
           

        }
    }

    IEnumerator GetVerionFromServer()
    {

        string dataUrl = domain + "Apps/" + AppName + "/getdata.php?app=" + AppName + "&action=get_version&rlt=" + GetTimeStamp();

        WWW wwwData = new WWW(dataUrl);
        yield return wwwData;


        if (!string.IsNullOrEmpty(wwwData.error))
        {

            LogMessage = "GetVerionFromServer www error :" + wwwData.error;
            Debug.Log(LogMessage);
            if (wwwData.error == "No Internet Connection")
            {

                // No Internet
                LogMessage = "Start Read ImageTarget JSON File.....";
                ReadImageTarget();

                GameAnalytics.NewDesignEvent("event:version:read");
            }
            else
            {
                yield break;
            }
        }
        else
        {

            string result = wwwData.text;
            //serverVersion = System.Convert.ToInt32(result);
            Version AppVersionObj = JsonUtility.FromJson<Version>(result);
            serverVersion = System.Convert.ToInt32(AppVersionObj.app_version);


            LogMessage = "LocalFile Version = " + version + " ; ServerFile Version = " + serverVersion;
            Debug.Log(LogMessage);

             //GameObject Loadingbar = GameObject.Find("Loadingbar") as GameObject;
            if (serverVersion != 0)
            {
                if (version != serverVersion)
                {
                    // Can upadte to latest version
                    GameObject loadindAnim = Resources.Load<GameObject>("LoadindAnim");
                    loadindAnim =  Instantiate(loadindAnim);
                    loadindAnim.name = "Loadingbar";

                    LogMessage = "Start Update ImageTarget JSON File.....";
                    UpdateFileHandler();

                    GameAnalytics.NewDesignEvent("event:version:update");
                }
                else
                {
                    // Current version is latest 

                    LogMessage = "Start Read ImageTarget JSON File.....";
                    ReadImageTarget();

                    GameAnalytics.NewDesignEvent("event:version:read");
                }
            }
            else
            {
                LogMessage = "There is no version to support.";
            }


        }

    }

    void UpdateFileHandler()
    {
        LogMessage = "Start Update Server File.....";

        Caching.ClearCache();

        ReseatDetectTable = true;

        DeleteFile(VersionFile);
        DeleteFile(ImageTargetJsonFile);

        DeleteFile(SettingFile);
       

        DownLoadDataSetDatabase();


    }

    void OnDataSetReady()
    {

        LogMessage = "Sync Version Number with Server .....";
        Debug.Log(LogMessage);

        version = serverVersion;
        string filePath = Application.persistentDataPath + "/" + VersionFile;
        StreamWriter newFile = new StreamWriter(filePath);
        newFile.WriteLine(serverVersion);
        newFile.Close();

        StartCoroutine(LoadSettingFileRequest());
       

        ReadImageTarget();
    }

    IEnumerator GetData()
    {

        string dataUrl = domain + "Apps/" + AppName + "/getdata.php?app=" + AppName + "&action=get_tag_data&rlt=" + GetTimeStamp();

        WWW wwwData = new WWW(dataUrl);
        yield return wwwData;


        if (!string.IsNullOrEmpty(wwwData.error))
        {

            LogMessage = "www error :" + wwwData.error;
            analyticsManager.ErrorEvent(LogMessage);
        }
        else
        {

            string fStr = wwwData.text;

            analyticsManager.DebugEvent(fStr);

            BuildImageTargtJson(fStr);
        }

        Debug.Log(LogMessage);
        wwwData.Dispose();

    }

    //===================================
    //Save JSON file for Image Target 
    void BuildImageTargtJson(string jsonStr)
    {
        string filePath = Application.persistentDataPath + "/" + ImageTargetJsonFile;

        //check if file exists
        if (!File.Exists(filePath))
        {

            LogMessage = "BuildImageTargtJson -> No " + ImageTargetJsonFile + " file exists, saving file...";

            //create new file
            StreamWriter newFile = new StreamWriter(filePath);
            newFile.WriteLine(jsonStr);
            newFile.Close();


            ReadImageTarget();

            RefreshEdiotrWindow();

        }
        else
        {

            LogMessage = "file already exists";
        }

        Debug.Log(LogMessage);

    }

    //===================================
    //Delete File  
    private void DeleteFile(string file)
    {
        string filePath = Application.persistentDataPath + "/" + file;

        //check iif file exists
        if (!File.Exists(filePath))
        {

            LogMessage = "no " + file + " file exists";

        }
        else
        {
            LogMessage = file + " file exists, deleting.....";

            File.Delete(filePath);
        }
        Debug.Log(LogMessage);

        RefreshEdiotrWindow();
    }

    // Read Image Target Data
    void ReadImageTarget()
    {
        string filePath = Application.persistentDataPath + "/" + ImageTargetJsonFile;

        if (File.Exists(filePath))
        {
            LogMessage = ImageTargetJsonFile + " file exist. Reading .....";

            string dataAsJson = File.ReadAllText(filePath);
            imageTargets = JsonHelper.FromJson<Target>(dataAsJson);
            Debug.Log(dataAsJson);


            CreateDetectTableFile(ReseatDetectTable);


            Debug.Log("ReadImageTarget count = " + imageTargets.Count<Target>());
            //Debug.Log("ReadImageTarget file_url = " + imageTargets[0].file_url);
            vuforia_ready = true;
            OnCloudEyeDataReady();
        }
        else
        {

            LogMessage = "ReadImageTarget-> No " + ImageTargetJsonFile + " file exist.";

            StartCoroutine(GetData());
        }


    }
    // Create Image Target Switch JSON
    void CreateDetectTableFile(bool reseat)
    {
        // Save Edit Load Detect Table File
        string filePath = Application.persistentDataPath + "/" + DetectTableFile;

        if (!File.Exists(filePath) || reseat)
        {

            int count = imageTargets.Count<Target>();
            detectTables = new DetectTable[count];
            for (var i = 0; i < count; ++i)
            {
                detectTables[i] = new DetectTable();
                detectTables[i].tg_name = imageTargets[i].tg_name;
                detectTables[i].detected = "0";
            }

            string detectTableJson = JsonHelper.ToJson(detectTables);

            StreamWriter detectTable = new StreamWriter(filePath);
            detectTable.WriteLine(detectTableJson);
            detectTable.Close();

            Debug.Log("CreateDetectTableFile detectTabkeJson----->" + detectTableJson);



        }
        else
        {

            // Test to udpate 
            //UpdateDetectTables("IMG_7943");
            ReadDetectTableFile();
        }

    }
    public void ReadDetectTableFile()
    {

        string filePath = Application.persistentDataPath + "/" + DetectTableFile;

        if (File.Exists(filePath))
        {


            string detectTableStr = File.ReadAllText(filePath);
            detectTables = JsonHelper.FromJson<DetectTable>(detectTableStr);
            Debug.Log("ReadDetectTable File detectTables =" + detectTableStr);



            for (var i = 0; i < detectTables.Count<DetectTable>(); ++i)
            {
                //Debug.Log("ReadDetectTable File detectTables : tg_name=" + detectTables[i].tg_name+ ", detected=" + detectTables[i].detected);
                float detected;
                float.TryParse(detectTables[i].detected, out detected);
                GameAnalytics.NewDesignEvent("markers:" + detectTables[i].tg_name, detected);
            }


        }
        else
        {
            Debug.Log("No Detect Table file exists");
        }

    }
    public void UpdateDetectTables(string target)
    {

        string filePath = Application.persistentDataPath + "/" + DetectTableFile;

        if (File.Exists(filePath))
        {

            string detectTableStr = File.ReadAllText(filePath);
            detectTables = JsonHelper.FromJson<DetectTable>(detectTableStr);

            for (var i = 0; i < detectTables.Count<DetectTable>(); ++i)
            {

                detectTables[i].tg_name = imageTargets[i].tg_name;
                if (detectTables[i].tg_name == target)
                {
                    detectTables[i].detected = "1";

                }

            }

            string detectTableJson = JsonHelper.ToJson(detectTables);
            StreamWriter detectTable = new StreamWriter(filePath);
            detectTable.WriteLine(detectTableJson);
            detectTable.Close();
            Debug.Log("UpdateDetectTables detectTableJson-->" + detectTableJson);
        }



    }

    void LoadSettingFile()
    {
         
        string filePath = Application.persistentDataPath + "/" + SettingFile;

     
        if (!File.Exists(filePath))
        {
            StartCoroutine(LoadSettingFileRequest());
        }
        else
        {
            ReadSettingFile();
        }

    }

    IEnumerator LoadSettingFileRequest()
    {
        string url = domain + "Apps/" + AppName + "/Targets/" + SettingFile;
        WWW wwwData = new WWW(url);
        Debug.Log("LoadSettingFileRequest url --->" + url);
        yield return wwwData;
       
        if (!string.IsNullOrEmpty(wwwData.error))
        {

            LogMessage = "LoadSettingFileRequest www error :" + wwwData.error;
            Debug.Log(LogMessage);
            analyticsManager.ErrorEvent(LogMessage);
            yield break;
        }
        else
        {

            string settingData = wwwData.text;
            Debug.Log("LoadSettingRequest settingData--->" + settingData);

            string filePath = Application.persistentDataPath + "/" + SettingFile;
            StreamWriter settingFileWriter = new StreamWriter(filePath);
            settingFileWriter.WriteLine(settingData);
            settingFileWriter.Close();

            ReadSettingFile();
        }
       
    }

    void ReadSettingFile()
    {
        string filePath = Application.persistentDataPath + "/" + SettingFile;
        //Debug.Log("ReadSettingFile filePath --->" + filePath);
        if (File.Exists(filePath))
        {
            string settingData = File.ReadAllText(filePath);
            settingList = JsonHelper.FromJson<Setting>(settingData);

            Debug.Log("ReadSettingFile settingData --->" + settingData);
        }

         
    }

    public Setting FilterMarkerSetting(string tg_name)
    {
        //Debug.Log(tg_name);
        foreach (Setting node in settingList)
        {
            if (node.tg_name == tg_name)
            {
                settingNode = node;
                break;
            }

        }
        return settingNode;
    }



    public string FilterBundleContentFilePath(string target_name)
    {
        content_url = "";

        for (var i = 0; i < imageTargets.Count<Target>(); ++i)
        {

            if (imageTargets[i].tg_name == target_name)
            {
                content_url = imageTargets[i].file_url;

                break;

            }
        }


        return content_url;
    }

    public string FilterBundleContentFileName(string target_name)
    {
        bundle_content = "";


        for (var i = 0; i < imageTargets.Count<Target>(); ++i)
        {

            if (imageTargets[i].tg_name == target_name)
            {
                bundle_content = imageTargets[i].file_name;
                break;
            }
        }

        return bundle_content;


    }

    public int FilterBundleContentIndex(string target_name)
    {
        int bundle_index = 0;

        for (var i = 0; i < imageTargets.Count<Target>(); ++i)
        {

            if (imageTargets[i].tg_name == target_name)
            {
                bundle_index = i;
                break;
            }
        }

        return bundle_index;
    }

    public int PraseVersionNumber(string target_name)
    {
        int version_no = 0;


        for (var i = 0; i < imageTargets.Count<Target>(); ++i)
        {

            if (imageTargets[i].tg_name == target_name)
            {
                version_no = Mathf.Abs(Animator.StringToHash(imageTargets[i].update_date));
                break;
            }
        }

        return version_no;
    }

 
    void DownLoadDataSetDatabase()
    {

        string datasetPath = domain + "Apps/" + AppName + "/Targets/0_DataSet/";
        string dataSetURl = datasetPath + AppName + ".dat";
        string xmlUrl = datasetPath + AppName + ".xml";
        string datFileName = AppName + ".dat";
        string xmlFileName = AppName + ".xml";
         
        StartCoroutine(DownloadFile(dataSetURl, datFileName));
        StartCoroutine(DownloadFile(xmlUrl, xmlFileName));
        OnDataSetReady();
    }

 
    IEnumerator DownloadFile(string urlPath, string fileName)
    {
        //Debug.Log("DownloadFile  urlPath= " + urlPath);
        UnityWebRequest uwr = new UnityWebRequest(urlPath);
        var path = Application.persistentDataPath + "/QCAR/"+ fileName;
        var dh = new DownloadHandlerFile(path);
        uwr.downloadHandler = dh;
        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError || uwr.isHttpError)
        {
            Debug.LogError(uwr.error);
            analyticsManager.ErrorEvent(uwr.error);
            yield break;
        }
        else
        {
            LogMessage = "File successfully downloaded and saved to " + path;
            Debug.Log(LogMessage);
        }

    }

    int GetTimeStamp()
    {

        System.DateTime epochStart = new System.DateTime(2018, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
        int timestamp = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;


        return timestamp;
    }

    void RefreshEdiotrWindow()
    {

        //UnityEditor.AssetDatabase.Refresh();

    }





}


public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.targets;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.targets = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.targets = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [System.Serializable]
    private class Wrapper<T>
    {
        public T[] targets;
    }
}