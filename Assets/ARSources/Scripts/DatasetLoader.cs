﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using System.Linq;
using UnityEngine.Networking;
using UnityEditor;
using UnityEngine.SceneManagement;
using GameAnalyticsSDK;
using System.IO;

public class DatasetLoader : MonoBehaviour
{

    CloudEyeManager cloudEye;


    string AssetBundleUrl;
    int AssetBundleVer;
    string AssetName;
    public string DataSetName;
    int imageTargetIndex = 0;
    string trackedName;
    string DataSetFile;

    AssetBundle bundle;
    GameObject AssetBundleObject = null;
    GameObject AssetBundleObjClone = null;
    RuntimeAnimatorController AniCtrl = null;
    List<GameObject> AssetBundleObjects = new List<GameObject>();
    List<RuntimeAnimatorController> AniCtrls = new List<RuntimeAnimatorController>();
    Target[] ImageTargets;
    Animator CurrentAnimator;
    ParticleSystem SandStarParticaleSys;


    float sandstarPending = 3.0f;
    bool timerLock = false;
    private IEnumerator PendingCoroutine;

    // Resources Assets 
    string ResourcesURL;
    string[] ResourcesNameArr = new string[] { "SandStarParticle", "StarParticle" };

    GameObject StarPrefeb;
    GameObject SandStarPrefab;
   
    bool SceneAssetLoaded = false;

 

    // Use this for initialization
    void Start()
    {


        cloudEye = GetComponent<CloudEyeManager>();
        CloudEyeManager.OnCloudEyeDataReady += SetupImageTargetData;
        cloudEye.CheckVersion();


        // If App has to reseat
        //cloudEye.DoReseat();

        //VuforiaARController.Instance.RegisterVuforiaInitializedCallback(OnVuforiaInit);
    }
    /*
    void OnVuforiaInit()
    {
        Debug.Log("VuforiaInitializedCallback");
    }
    */

    void SetupImageTargetData()
    {


        ImageTargets = cloudEye.imageTargets;
        //Debug.Log("DatasetLoader file_url = " + imageTargets[0].file_url);
        if (ImageTargets.Count<Target>() > 0)
        {
            AssetBundleUrl = cloudEye.FilterBundleContentFilePath(ImageTargets[imageTargetIndex].tg_name);
            AssetName = cloudEye.FilterBundleContentFileName(ImageTargets[imageTargetIndex].tg_name);
            AssetBundleVer = cloudEye.PraseVersionNumber(ImageTargets[imageTargetIndex].tg_name);
            StartCoroutine(LoadAssetBundle());

            Debug.Log("SetupImageTargetData   AssetName=" + AssetName);

        }
    }

    IEnumerator LoadAssetBundle()
    {
        while (!Caching.ready)
        {
            yield return null;
        }

        //----Unity Editor-----
        //Hash128 hash;
        //uint crc;
        //BuildPipeline.GetHashForAssetBundle(AssetBundleUrl, out hash);
        //BuildPipeline.GetCRCForAssetBundle(AssetBundleUrl, out crc);
        string bundleFilePath = AssetBundleUrl + "/iPhone/content";
        Debug.Log("bunndleFilePath=" + bundleFilePath);
        using (var www = WWW.LoadFromCacheOrDownload(bundleFilePath, AssetBundleVer))
        {
            yield return www;

            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.Log(www.error);
                yield return null;
            }
            else
            {


                // Get downloaded asset bundle
                bundle = www.assetBundle;

                if (bundle.isStreamedSceneAssetBundle)
                {
                    Debug.Log("Load Scene -------");
                    // Load Scene Asset  
                    string[] scenePaths = bundle.GetAllScenePaths();
                    string sceneName = System.IO.Path.GetFileNameWithoutExtension(scenePaths[0]);

                    SceneAssetLoaded = false;
                    int loadedIndex = 0;
                    for (int index = 0; index < AssetBundleObjects.Count<GameObject>(); ++index)
                    {
                        Debug.Log("AssetBundleObjects[] name ---->" + AssetBundleObjects[index].name);
                        if (AssetBundleObjects[index].name == AssetName)
                        {
                            SceneAssetLoaded = true;
                            loadedIndex = index;
                            break;
                        }
                    }

                    if (SceneAssetLoaded)
                    {
                        Debug.Log("loaded Index---->" + loadedIndex);
                        // Scence Asset Loaded into the scene
                        AssetBundleObject = AssetBundleObjects[loadedIndex];
                        AniCtrl = AssetBundleObject.GetComponent<RuntimeAnimatorController>();
                        if (!AniCtrl)
                        {
                            AniCtrl = null;
                        }

                    }
                    else
                    {
                        // Scence Asset is Loading into the scene
                        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

                        while (!asyncLoad.isDone)
                        {
                            yield return null;
                        }
                        Scene ARScene = SceneManager.GetActiveScene();
                        Scene AssetBundleScene = SceneManager.GetSceneByName(sceneName);
                        GameObject[] RootObjects = AssetBundleScene.GetRootGameObjects();
                        Debug.Log(" LoadAssetBundle  AssetName--->" + AssetName);
                        foreach (GameObject obj in RootObjects)
                        {

                            if (obj.name.ToLower() == AssetName)
                            {
                                SceneAssetLoaded = true;
                                AssetBundleObject = obj;
                                AssetBundleObject.name = AssetName;
                                AniCtrl = AssetBundleObject.GetComponent<RuntimeAnimatorController>();
                                if (!AniCtrl)
                                {
                                    AniCtrl = null;
                                }
                                SceneManager.MoveGameObjectToScene(obj, ARScene);
                                SceneManager.UnloadSceneAsync(AssetBundleScene);
                                AssetBundleObject.SetActive(false);
                            }

                        }
                    }

                }
                else
                {
                    // Load Asset Bundle
                    Debug.Log("Load Asset Bundle -------");
                    AssetBundleRequest bundleRequest = bundle.LoadAssetAsync(AssetName, typeof(GameObject));
                    AssetBundleObject = bundleRequest.asset as GameObject;
                    AssetBundleObject.name = AssetName;

                    AssetBundleRequest ctrlBundleRequest = bundle.LoadAssetAsync(AssetName, typeof(RuntimeAnimatorController));
                    if (ctrlBundleRequest != null)
                    {
                        AniCtrl = ctrlBundleRequest.asset as RuntimeAnimatorController;

                    }
                    else { AniCtrl = null; };
                }
            }
        }

        if (AssetBundleObject)
        {

            AssetBundleObjects.Add(AssetBundleObject);
            if (AniCtrl)
            {
                AniCtrls.Add(AniCtrl);
            }
            else { AniCtrls.Add(null); }

            imageTargetIndex++;
            Debug.Log("LoadAssetBundle   imageTargetIndex=" + imageTargetIndex);

            if (imageTargetIndex < ImageTargets.Count<Target>())
            {
                string nextAssetName = cloudEye.FilterBundleContentFileName(ImageTargets[imageTargetIndex].tg_name);

                //Debug.Log("nextAssetName " + nextAssetName);
                for (int k = 0; k < AssetBundleObjects.Count; ++k)
                {
                    if (AssetBundleObjects[k].name == nextAssetName)
                    {
                        //Asset already loaded
                        Debug.Log("Found Asset Loaded " + nextAssetName);
                        AssetBundleObjects.Add(AssetBundleObjects[k]);

                        if (nextAssetName == "coin")
                        {
                            AniCtrls.Add(AniCtrls[k]);
                        }
                        else { AniCtrls.Add(null); }

                        imageTargetIndex++;
                        break;
                    }
                }

                if (imageTargetIndex == ImageTargets.Count<Target>())
                {
                    string resourcesdePath = cloudEye.domain + "Apps/" + cloudEye.AppName + "/Resources/resources";
                    SetupResourceAsset(resourcesdePath);
                }
                else
                {
                    SetupImageTargetData();

                }

                bundle.Unload(false);


            }
            else
            {
                // Completed Setup AssetBundle
                Debug.Log("LoadAssetBundle   Completed Setup AssetBundle");


                string resourcesdePath = cloudEye.domain + "Apps/" + cloudEye.AppName + "/Resources/resources";
                SetupResourceAsset(resourcesdePath);


            }

        }
        else
        {
            Debug.Log("LoadAssetBundle   Error");

        }





    }
    void SetupResourceAsset(string path)
    {



        StartCoroutine(LoadResourcesAsset(path));


    }


    IEnumerator LoadResourcesAsset(string resourceURL)
    {
        while (!Caching.ready)
        {
            yield return null;
        }

        using (var www = WWW.LoadFromCacheOrDownload(resourceURL, AssetBundleVer))
        {

            yield return www;


            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.Log(www.error);
                yield break;

            }
            else
            {

                bundle = www.assetBundle;

                if (bundle.isStreamedSceneAssetBundle)
                {
                    string[] scenePaths = bundle.GetAllScenePaths();
                    string sceneName = System.IO.Path.GetFileNameWithoutExtension(scenePaths[0]);
                    Debug.Log("Scene Name = " + sceneName);

                    AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

                    while (!asyncLoad.isDone)
                    {
                        yield return null;
                    }
                    Scene ARScene = SceneManager.GetActiveScene();
                    Scene AssetBundleScene = SceneManager.GetSceneByName(sceneName);
                    GameObject[] RootObjects = AssetBundleScene.GetRootGameObjects();

                    foreach (GameObject obj in RootObjects)
                    {


                        if (obj.name == ResourcesNameArr[0])
                        {
                            SandStarPrefab = obj;
                            SceneManager.MoveGameObjectToScene(obj, ARScene);

                        }

                        if (obj.name == ResourcesNameArr[1])
                        {
                            StarPrefeb = obj;
                            StarPrefeb.SetActive(false);
                            SceneManager.MoveGameObjectToScene(obj, ARScene);

                        }
                    }

                    SceneManager.UnloadSceneAsync(AssetBundleScene);
                }


            }
        }


        bundle.Unload(false);

        // Completed Load Resources Asset Bundle then setup dataset
        SetupDataSet();



    }

    void SetupDataSet()
    {

        string directoryDataSet = Application.persistentDataPath + "/QCAR/";
        DataSetFile = directoryDataSet + DataSetName + ".xml";
        bool dataSetExists = DataSet.Exists(DataSetFile, VuforiaUnity.StorageType.STORAGE_ABSOLUTE);


        if (dataSetExists)
        {

            // Registering call back to know when Vuforia is ready
            VuforiaARController.Instance.RegisterVuforiaStartedCallback(OnVuforiaStarted);

        }
        else
        {
            // No DataSet file exists , download dataset database
            //cloudEye.DownLoadDataSetDatabase();
        }

    }


    // This function is called when vuforia gives the started callback
    void OnVuforiaStarted()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "ARTracking");

        bool status = LoadDataSet(DataSetFile);

        if (status)
        {
            Debug.Log("Dataset Loaded");


        }
        else
        {

            Debug.LogError("<color=yellow>Failed to load dataset: '" + DataSetName + "'</color>");
        }



    }


    // Load and activate a data set at the given path.
    private bool LoadDataSet(string dataSetPath)
    {
        // Request an ImageTracker instance from the TrackerManager.
        ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
        IEnumerable<DataSet> dataSetList = objectTracker.GetActiveDataSets();
        foreach (DataSet set in dataSetList.ToList())
        {
            objectTracker.DeactivateDataSet(set);
        }


        // Create a new empty data set.
        DataSet dataSet = objectTracker.CreateDataSet();
        if (dataSet.Load(dataSetPath, VuforiaUnity.StorageType.STORAGE_ABSOLUTE))
        {
            // stop tracker so that we can add new dataset
            objectTracker.Stop();

            if (!objectTracker.ActivateDataSet(dataSet))
            {
                // Note: ImageTracker cannot have more than 100 total targets activated
                //"<color=yellow>Failed to Activate DataSet: " + dataSetPath + "</color>";
                cloudEye.LogMessage = "Failed to Activate DataSet: " + dataSetPath;
            }


            if (!objectTracker.Start())
            {
                cloudEye.LogMessage = "Tracker Failed to Start.";

            }

            Debug.Log(cloudEye.LogMessage);

            try
            {
                GameObject Loadingbar = GameObject.Find("Loadingbar") as GameObject;
                if (Loadingbar != null)
                    Loadingbar.SetActive(false);

            }
            catch { Debug.Log("There is no Loading Animation !!"); }


            AttachContentToTrackables();

            return true;
        }
        else
        {

            Debug.Log("Failed to load dataset: " + dataSetPath);

            return false;
        }



    }

    // Add Trackable event handler and content (cubes) to the Targets.
    private void AttachContentToTrackables()
    {
        // get all current TrackableBehaviours
        IEnumerable<TrackableBehaviour> trackableBehaviours =
        TrackerManager.Instance.GetStateManager().GetTrackableBehaviours();


        // Loop over all TrackableBehaviours.
        foreach (TrackableBehaviour tb in trackableBehaviours)
        {
            if (tb.name == "New Game Object")
            {
                //ImageTargetBehaviour ImgTargetBehaviour =  tb.GetComponent<ImageTargetBehaviour>();

                //int id = ++counter;
                // change generic name to include trackable name
                tb.gameObject.name = tb.TrackableName;

                // add additional script components for trackable
                tb.gameObject.AddComponent<DefaultTrackableEventHandler>();
                tb.gameObject.AddComponent<TurnOffBehaviour>();
                //tb.gameObject.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);

                cloudEye.LogMessage += tb.TrackableName + "; ";



                // instantiate AssetBundleObject object and parent to trackable
                if (AssetBundleObject != null)
                {
                    //Debug.Log("AssetBundleObject Loaded");

                    /*
                    GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    cube.transform.parent = tb.gameObject.transform;
                    //cube.transform.position = new Vector3(0, 0, 0);
                    cube.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    */


                    GameObject SandStarParticaleObj = Instantiate(SandStarPrefab);
                    SandStarParticaleObj.name = tb.TrackableName + "_SandStar";
                    SandStarParticaleObj.transform.parent = tb.gameObject.transform;

                    Setting settingNode = cloudEye.FilterMarkerSetting(tb.TrackableName);
                    float localscale = float.Parse(settingNode.scale);
                    SandStarParticaleObj.transform.localScale = new Vector3(localscale, localscale, localscale);
                    SandStarParticaleSys = SandStarParticaleObj.GetComponent<ParticleSystem>();

                    float shapeX = float.Parse(settingNode.shape_x);
                    float shapeY = float.Parse(settingNode.shape_y);
                    float shapeZ = float.Parse(settingNode.shape_z);
                    var shape = SandStarParticaleSys.shape;
                    shape.scale = new Vector3(shapeX, shapeY, shapeZ);

                    //SandStarParticaleObj.transform.localPosition = SandStarPrefab.transform.localPosition;
                    SandStarParticaleSys = SandStarParticaleObj.GetComponent<ParticleSystem>();
                    SandStarParticaleSys.Stop();
                    Destroy(SandStarPrefab);



                }
                else
                {
                    Debug.Log("No AssetBundleObject");
                }

            }


        }


        DefaultTrackableEventHandler.OnTargetFound += OnTargetFound;
        DefaultTrackableEventHandler.OnTargetLost += OnTargetLost;
    }



    void OnTargetFound(TrackableBehaviour trackableBehave)
    {


        trackedName = trackableBehave.TrackableName;
        string detected = "0";
        timerLock = false;


        DetectTable[] detectTables = cloudEye.detectTables;
        foreach (DetectTable table in detectTables)
        {
            if (table.tg_name == trackedName)
            {
                detected = table.detected;
                break;
            }

        }
        //ToDo Set Condition here to restrict within each markers detected by once
        //if(detected == "0")
        //{

        // Play SandStar
        GameObject SandStarObj = GameObject.Find(trackedName + "_SandStar");
        SandStarParticaleSys = SandStarObj.GetComponent<ParticleSystem>();
        SandStarParticaleSys.Play();
        AssetBundleStandBy();

        AudioSource audiosource = SandStarObj.GetComponent<AudioSource>();
        audiosource.enabled = true;
        audiosource.Play();


        GameAnalytics.NewDesignEvent("detected:" + trackedName);
        GameAnalytics.SetCustomDimension01(trackedName);

        // }


    }

    void OnTargetLost()
    {
        ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
        if (objectTracker.IsActive)
        {

            if (PendingCoroutine != null)
            {
                Debug.Log("StopCoroutine");

                // Stop Sand Star Particle
                SandStarParticaleSys.Stop();
                SandStarParticaleSys.Clear();
                StopCoroutine(PendingCoroutine);

                GameObject SandStarObj = GameObject.Find(trackedName + "_SandStar");
                AudioSource audiosource = SandStarObj.GetComponent<AudioSource>();
                audiosource.Stop();
                audiosource.enabled = false;
            }

        }
        else
        {
            // Stop Tracking
            if (SandStarParticaleSys != null)
            {

                // Stop Sand Star Particle
                SandStarParticaleSys.Stop();
                SandStarParticaleSys.Clear();
                StopCoroutine(PendingCoroutine);

            }



        }



    }

    void AssetBundleStandBy()
    {


        PendingCoroutine = SandStarPending(sandstarPending);
        StartCoroutine(PendingCoroutine);
    }

    IEnumerator SandStarPending(float time)
    {


        yield return new WaitForSeconds(time);
        // Stop tracking after {time} sec
        ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
        objectTracker.Stop();

        SpawnAssetBundle();

        // Update Detect Table
        cloudEye.UpdateDetectTables(trackedName);


        SandStarParticaleSys.Stop();
        SandStarParticaleSys.Clear();

       



    }



    void SpawnAssetBundle()
    {
        string lodMsg = "";

        GameObject ARCamObj = GameObject.Find("ARCamera") as GameObject;
        ARCamObj.transform.rotation = Quaternion.identity;
        ARCamObj.transform.position = new Vector3(0.0f, 0.0f, 0.0f);

        int assetIndex = cloudEye.FilterBundleContentIndex(trackedName);
        AssetBundleObject = AssetBundleObjects[assetIndex];

        AssetBundleObjClone = Instantiate(AssetBundleObject);
        AssetBundleObjClone.name = trackedName + "_" + AssetBundleObject.name;
        //Vector3 CentrePos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, Camera.main.nearClipPlane + 21));
        //AssetBundleObjClone.transform.position = CentrePos;
        //SetLayerOnAllRecursive(AssetBundleObjClone, 9);
        Debug.Log("SpawnAssetBundle --->" + AssetBundleObjClone.name);


        lodMsg += "; ARCamObj position: " + ARCamObj.transform.position.ToString();
        lodMsg += "; Camera.main position: " + Camera.main.transform.position.ToString();
        lodMsg += "; AssetBundleObjClone position: " + AssetBundleObjClone.transform.position.ToString();
        //Debug.Log(lodMsg);

        /* Second Camera Testing     
        //Camera RenderCam = GameObject.FindWithTag("AssetBundleCamera").GetComponent<Camera>();
        //Vector3 CentrePos = RenderCam.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, RenderCam.nearClipPlane+30));
        */



        //AssetBundleObjClone.transform.rotation = BgPlane.gameObject.transform.rotation;
        //AssetBundleObjClone.transform.Rotate(Vector3.right, AssetBundleObjClone.transform.rotation.x+90);
        //AssetBundleObjClone.transform.localScale = new Vector3(50.0f, 50.0f, 50.0f);

        //ToDo For Coin Sequence Images
        //AssetBundleObjClone.AddComponent<AnimationBehaviour>();

        AssetBundleObjClone.AddComponent<AnimateManager>();
        CurrentAnimator = AssetBundleObjClone.GetComponent<Animator>();
        if (AniCtrls[assetIndex])
            CurrentAnimator.runtimeAnimatorController = AniCtrls[assetIndex];


        GameAnalytics.NewDesignEvent("detected:" + trackedName);

        if (AssetBundleObject.name == "coin")
        {


            PendingCoroutine = CollisionPending();
            StartCoroutine(PendingCoroutine);

            StarPrefeb.SetActive(true);
            GameObject StarParticaleObj = Instantiate(StarPrefeb);
            StarParticaleObj.name = trackedName + "_Star";
            StarParticaleObj.transform.parent = AssetBundleObjClone.gameObject.transform;
            StarParticaleObj.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
            ParticleSystem StarParticaleSys = StarParticaleObj.GetComponent<ParticleSystem>();
            StarParticaleSys.Stop();
            StarPrefeb.SetActive(false);



            GameAnalytics.NewDesignEvent("asset:coin");
        }
        else
        {
            GameAnalytics.NewDesignEvent("asset:skyblue");
        }


    }

    IEnumerator CollisionPending()
    {

        yield return new WaitForSeconds(2.0f);
        BoxCollider AssetCollider = AssetBundleObjClone.AddComponent<BoxCollider>();
        AssetCollider.size = new Vector3(4.0f, 4.0f, 4.0f);

    }

    void SetLayerOnAllRecursive(GameObject obj, int layer)
    {
        obj.layer = layer;
        foreach (Transform child in obj.transform)
        {
            SetLayerOnAllRecursive(child.gameObject, layer);
        }
    }

    public string GetMarkerDetected()
    {

        //if trachedName == "" is nothing been detected , else return marker ID
        return trackedName;

    }

}
