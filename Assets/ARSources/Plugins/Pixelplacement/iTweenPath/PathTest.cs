﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathTest : MonoBehaviour {

	// Use this for initialization
	void Start () {

        GameObject obj = GameObject.Find("Cube");

        Vector3[] paths = iTweenPath.GetPath("Path");
        //paths[0] = AssetFound.transform.position;
        //paths[paths.Length - 1] = position;

        Hashtable moveHt = new Hashtable();
        moveHt.Add("path", paths);
        moveHt.Add("time", 1);
		moveHt.Add("speed", 25);
		moveHt.Add("looptype", iTween.LoopType.loop);
        //moveHt.Add("delay", 0.25);
        //moveHt.Add("easetype", iTween.EaseType.easeInOutQuad);
        moveHt.Add("easetype", iTween.EaseType.spring);
        moveHt.Add("onstart", "onTweenStart");
        moveHt.Add("oncomplete", "onTweenComplete");
        moveHt.Add("oncompletetarget", obj);
        iTween.MoveTo(obj, moveHt);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
