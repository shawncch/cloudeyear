﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using GameAnalyticsSDK;

public class AnimateManager : MonoBehaviour
{

    /*
    public delegate void AnimationEvt();
    public delegate void TouchBehaveEvt(string touched_name);
    public static event AnimationEvt OnAnimateStandby;
    public static event TouchBehaveEvt OnTouchedHandler;
    */


    //ParticleSystem PS;
    //public Mesh PSMesh;
    GameObject TweenPathObj;
    GameObject StarParticaleObj;
    Animator CurrentAnimator;

    //private IEnumerator PenddingCoroutine = null;

    private void Start()
    {
        //PS = GetComponent<ParticleSystem>();

        string gameobjName = this.gameObject.name;
        int index = gameobjName.IndexOf("coin", System.StringComparison.Ordinal);
        Debug.Log("AnimateManager  index=" + index);
        if (index != -1)
        {
            // coin asset
            AddAnimationPath();
        }


    }
    private void Update()
    {

     

        ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
        if (objectTracker.IsActive)
        {
            objectTracker.Stop();
        }

        try
        {
           GameObject ARCamera = GameObject.Find("ARCamera");
           
            //Debug.Log("ARCamera activeInHierarchy: " + ARCamera.activeInHierarchy);
        }
        catch
        {
            Debug.Log("Cannnot Find ARCamera !!");
            Destroy(this.gameObject);
            GameAnalytics.NewDesignEvent("event:StopARCamera");
        }

    }

    void AddAnimationPath()
    {
        TweenPathObj = GameObject.Find("AnimPath");
        TweenPathObj.SetActive(false);
        GameObject TweenPathObjClone = Instantiate(TweenPathObj);
        TweenPathObjClone.transform.parent = this.gameObject.transform;
        iTweenPath TweenPath = TweenPathObjClone.GetComponent<iTweenPath>();
        TweenPath.pathName = "PathClone";
        TweenPathObjClone.SetActive(true);


    }

    int round = 0;
    bool touched = false;
    //Evnet Triggered by Animation
    void WaitingForTapping()
    {
        Debug.Log("WaitingForTapping");
        GameObject AssetFound = this.gameObject;


        CurrentAnimator = AssetFound.GetComponent<Animator>();
        //ToDo For Coin Sequence Images was disable, MUST Play in standard stat
        CurrentAnimator.Play("Jump", 0, 0.476f);

        round++;

        if (round == 1 && !touched)
        {

            touched = true;
            OnTouched(this.gameObject.name);
        }

        /*IF the coin has to wait  
        if (PenddingCoroutine == null)
        {

            //PenddingCoroutine = OnStartWaitingToTap();
            //StartCoroutine(PenddingCoroutine);

        }
        */



    }

    /*IF the coin has to wait  
    IEnumerator OnStartWaitingToTap()
    {
        yield return new WaitForSeconds(6.0f);
        GameAnalytics.NewDesignEvent("event:skip_tap");
        OnTouched(this.gameObject.name);

    }
    */


    void OnTouched(string touched_obj_name)
    {
        /*IF the coin has to wait  
        try
        {
            StopCoroutine(PenddingCoroutine);
            PenddingCoroutine = null;
        }
        catch
        {
            Debug.Log("Stop PenddingCoroutine, but PenddingCoroutine is null");
        }
        */

        //Debug.Log("OnTouched----> touched_obj_name=" + touched_obj_name);


        GameObject AssetFound = this.gameObject;
        CurrentAnimator = AssetFound.GetComponent<Animator>();
        //CurrentAnimator.Play("Take 001", 0, 0.0f);


        //Vector3 position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.farClipPlane));
        //Debug.Log("Coin Begin Position ---> " + AssetFound.transform.position.x + "; " + AssetFound.transform.position.y + "; " + AssetFound.transform.position.z);
        //Debug.Log("Coin End Position ---> "+position.x +"; "+ position.y +"; "+position.z);


        string[] name_list = touched_obj_name.Split(char.Parse("_"));
        string touched_asset = name_list[name_list.Length - 1];
        if (touched_asset == "coin")
        {
            // Touched Coin



            string touched_asset_name = touched_obj_name.Replace("_coin", "");
            StarParticaleObj = GameObject.Find(touched_asset_name + "_Star");
            ParticleSystem StarParticaleSys = StarParticaleObj.GetComponent<ParticleSystem>();
            StarParticaleObj.transform.position = AssetFound.transform.position;
            StarParticaleSys.Play();

            // Play Star Audio Effect
            //AudioSource audiosource = StarParticaleObj.GetComponent<AudioSource>();
            //audiosource.Play();
           


            Vector3[] paths = iTweenPath.GetPath("PathClone");
            //paths[0] = AssetFound.transform.position;
            //paths[paths.Length - 1] = position;

            Hashtable moveHt = new Hashtable();
            moveHt.Add("path", paths);
            moveHt.Add("time", 1);
            moveHt.Add("speed", 25);
            moveHt.Add("onstart", "onTweenStart");
            moveHt.Add("oncomplete", "onTweenComplete");
            moveHt.Add("oncompletetarget", AssetFound);
            iTween.MoveTo(AssetFound, moveHt);

            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "ARTracking", "Coin");
        }
        else
        {

            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "ARTracking", "SkyBlueMonster");
        }


        /* Get iTweenPath Testing
         * ht.Add("path",iTweenPath.GetPath(""));
         * ht.Add("time",5);
         * ht.Add("easetype",iTween.EaseType.easeInOutSinde);
         *
         */




    }
    void onTweenStart()
    {

        CurrentAnimator.speed = 20.0f;

        StartCoroutine(onTimerComplete());


    }
    void onTweenComplete()
    {

        GameObject AssetFound = this.gameObject;
        CurrentAnimator.enabled = false;
        iTween.Init(AssetFound);


        ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
        objectTracker.Start();

        Destroy(AssetFound);
        TweenPathObj.SetActive(true);

    }



    IEnumerator onTimerComplete()
    {

        yield return new WaitForSeconds(1.0f);
        StarParticaleObj.SetActive(false);

    }




}
