﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameAnalyticsSDK;

public class AnalyticsManager : MonoBehaviour {

    // Use this for initialization
    private void Awake()
    {
        
        GameAnalytics.Initialize();
         
        StartCoroutine(CheckGamAnalytivsActive());

    }

    IEnumerator CheckGamAnalytivsActive()
    {

        CloudEyeManager cloudEyeManager = GetComponent<CloudEyeManager>();
        string domain = cloudEyeManager.domain;
        string url = domain + "Apps/" + cloudEyeManager.AppName + "/getdata.php?&action=game_analytics";
        WWW www = new WWW(url);
        yield return www;

        if(!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("CheckGamAnalytivsActive :"+ www.error);
            yield break;
        }
        else{
           
            bool  active = System.Convert.ToBoolean(www.text);
            Debug.Log("CheckGamAnalytivsActive :"+ active);

            if(!active)
            {
                GameAnalytics.EndSession();

            }
            else{
                GameAnalytics.StartSession();
            }
        }


    }

    void Start () {
       
    }
	
    public void ErrorEvent(string msg)
    {
        GameAnalytics.NewErrorEvent(GAErrorSeverity.Error, msg);

    }

    public void DebugEvent(string msg)
    {

        GameAnalytics.NewErrorEvent(GAErrorSeverity.Debug, msg);
    }
	 
}
