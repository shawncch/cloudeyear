﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LunarManager : MonoBehaviour {

	// Use this for initialization
	void Start () {

        StartCoroutine(CheckLunarDebugActive());
       
	}
	
	 IEnumerator CheckLunarDebugActive()
    {
        CloudEyeManager cloudEyeManager = GameObject.Find("CloudEyeAR").GetComponent<CloudEyeManager>();
        string url = cloudEyeManager.domain + "Apps/" + cloudEyeManager.AppName + "/getdata.php?action=game_debug";
        WWW www = new WWW(url);
        yield return www;


        if(!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("CheckLunarDebugActive Error: " + www.error);
            this.gameObject.SetActive(true);
            yield break;
        }
        else{
            bool active = System.Convert.ToBoolean(www.text);

            Debug.Log("CheckLunarDebugActive active: " + active);
            if (!active)
            {
                this.gameObject.SetActive(false);
            }

        }


    }
}
