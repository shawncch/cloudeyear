﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegisterTouchBehaviour : MonoBehaviour
{

    // Use this for initialization
      string touched_obj_name;

       Camera RenderCam;

    void Start()
    {
        // RenderCam = GameObject.FindWithTag("AssetBundleCamera").GetComponent<Camera>();
        RenderCam = Camera.main.GetComponent<Camera>();
    }
  
    void Update()
    {
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0))
        {
           
            Ray ray = RenderCam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider != null)
                {
                    hit.collider.enabled = false;
                    touched_obj_name = hit.transform.gameObject.name;
                    hit.transform.gameObject.SendMessage("OnTouched", touched_obj_name);
                 
                }
            }
        }


        for (int i = 0; i < Input.touchCount; ++i)
        {
            if (Input.GetTouch(i).phase.Equals(TouchPhase.Began))
            {
                // Construct a ray from the current touch coordinates
                Ray ray = RenderCam.ScreenPointToRay(Input.GetTouch(i).position);
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider != null)
                    {
                       
                        hit.collider.enabled = false;
                        touched_obj_name = hit.transform.gameObject.name;
                        hit.transform.gameObject.SendMessage("OnTouched", touched_obj_name);

                    }

                }
            }
        }

    }

}
